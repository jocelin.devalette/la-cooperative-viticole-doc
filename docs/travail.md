# Travail à réaliser

## Objectifs de travail 

Cette Page cite les objectifs a atteindre, certains sont simples, d'autres plus complexes et vous demanderons certainement d'aller faire des recherches sur internet. 

A chaque objectif est associé un nombre de points attribué si l'objectif est atteint
## Objectifs liès à la base de données

!!! danger "Objectif N°1 - 1 point"
    Insérer en ligne de commande, un ou deux nouveaux viticulteurs

!!! danger "Objectif N°2 - 1 point"
    Insérer en ligne de commande, un ou deux nouveaux vins"

!!! danger "Objectif N°3 - 1 point"
    Modifier le mot de passe du buveur Macquart, il l'a oublié.

!!! danger "Objectif N°4 - 1 point" 
    Modifier le stock du vin Chateau Latour, il ne reste que 3 bouteilles de ce vin.

!!! danger "Objectif N°5 - 2 points"
    Insérer une nouvelle commande du buveur Macquart qui a commandé 2 bouteilles de Chateau Latour, et qui attend d'être livré

## Objectifs liès au site web
!!! danger "Objectif N°1 - 1 point"
    Modifier le fichier `index.html`, pour que dans le pied de page s'affiche les noms des membres du groupe.

!!! danger "Objectif N°2 - 2 points"
    Quand on saisit du texte dans un formulaire, celui-ce est de couleur rouge, cette mise en forme est gérée par le fichier css. Modifier la feuille de style `css/cooperative.css`, pour que ce texte s'affiche en bleu.

!!! danger "Objectif N°3 -  2 points"
    Supprimer le texte **Activité 1.3 Implémenter une base de données** et modifier les couleurs de la page `index.html`.

!!! danger "Objectif N°3bis - 3 points"
    Remplacer le texte *Ce script est exécuté sur le serveur Apache/2.4.38 (Debian) qui se trouve à l'adresse 10.1.0.251.* par le nom et le prénom de l'utilisateur lorsqu'il est connecté.

!!! danger "Objectif N°4 - 3 points"
    Modidifier le formulaire du fichier `commande.php` pour qu'il demande la quantité commandée pour chaque vin sous forme d'un nombre saisi au clavier.

!!! danger "Objectif N°5 - 4 points"
    Modifier le fichier `recap.php` pour que cette quantité commandée soit enregistrée dans la base de donnée.

!!! danger "Objectif N°6 - 4 points"
    Vérifiez que la commande du vin ne peut être passée que si la quantité commandée est plus petite que le stock. Si c'est le cas le site doit afficher **Commande impossible - Stock insuffisant**.

!!! danger "Objectif N°7 - 4 points"
    Quand la commande est passée déduisez du stock laquantité commandée.

!!! danger "Objectif N°8 - 5 points"
    Créer une nouvelle page `viticulteurs.php` qui permette à la secrétaire de saisir un nouveau viticulteur directement depuis le site web. Cette page devra respecter la charte graphique du site.

!!! danger "Objectif N°9 - X points"
    Vous avez toute liberté pour modifier le site à condition qu'il reste opérationnel.
