# Terminale NSI - Projet Coopérative Viticole

!!! danger "Objectif de l'activité"
    L'objectif de cette activité est de déployer un site internet interfacé avec une base de données pour une coopérative viticole.

    Un premier développeur a commencé a travailler sur ce projet,mais il donnait pas satisfaction, vous êtes donc chargé de reprendre le projet et de le modifier selon le cahier des charges communiqué par le directeur de la coopérative.

## Etape 1: Récupération du projet.

!!! info "Connexion sur le serveur en ssh"
    Chaque élève dispose d'un compte sur le serveur, il appartient également à un groupe. Le site sera déployé dans le répertoire du groupe.

    Dans l'exemple ci-dessous le groupe contient trois membres *test1*,*test2* et *test3*. Le site lui devra être déployé dans le repertoire du groupe *groupetest*.
    ```terminal
    drwxrwx--x  3 groupetest groupetest 4096 sept. 18 20:06 groupetest
    drwxrwx--x  2 test1      groupetest 4096 sept. 18 18:32 test1
    drwxrwx--x  2 test2      groupetest 4096 sept. 18 17:02 test2
    drwxrwx--x  4 test3      groupetest 4096 sept. 18 20:13 test3
    ```

    !!! question "Question 1"

    === "Question"
        Comment sait-on que dans le listing ci-dessus il s'agit de répertoires ?

    === "Réponse"
        Dans le listing ci-dessus on voit que chaque ligne `drwxrwx--x` commence par un **d**, pour directory.

!!! info "Récupération de l'archive"
    L'archive se trouve sur Gitlab pour la récupérer :

    ```sh
    git clone https://gitlab.com/jocelin.devalette/la-cooperative-viticole.git
    ```

    Il conviendra ensuite de copier le répertoir `public_html` dans le répertoire de groupe pour déployer le site.

    ```sh
    mv la-cooperative_viticole/public_html/ /home/groupetest/public_html
    ```

## Connexion à la base de données.

!!! info "Informations de connection"
    Les informations de connexion à la base de données se trouvent dans le fichier `conn.php`. Il conviendra de les modifier avec ceux communiqués par le professeur :

    ```sh
    nano conn.php
    ```

    Vous modifierez les lignes `$username` , `$password` et `$database` 

    ```php

    <?php
    function OpenCon()
        {		
                $servername = 'localhost';
                // Modifiez avec les données communiquées par le professeur
                $username = 'admin';
                $password = 12345 ;
                $database = 'groupe';
                
                //On établit la connexion
                $conn = mysqli_connect($servername, $username, $password, $database);
                
                //On vérifie la connexion
                if($conn->connect_error){
                    die('Erreur : ' .$conn->connect_error);
                }
            return $conn;
        }
        ?>
    ```

    !!! question "Question 2"

    === "Question"
        Comment reconnait-on les variables en php?

    === "Réponse"
        Elles sont précédées du symbole `$`

    !!! question "Question 3"

    === "Question"
        Quelle est la structure d'une instruction conditionnelle en php ?

    === "Réponse"     
        La structure d'une instruction conditionnelle est la suivante :
        ```php
        <? php
        if (condition) 
        {instruction}
        else 
        {instruction}
        ?>
        ```




   

   



    
    



