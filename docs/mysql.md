# La base de données Mysql

## Se connecter à Mysql

!!! info "Se connecter à Mysql en ligne de commande"

    Pour se connecter à mysql en ligne de commande il suffit de taper :

    ```sh
    mysql -u nom_utilisateur -p
    ```

    Il convient ensuite de signaler la base utilisée :

    ```sh
    mysql> USE gtest
    Reading table information for completion of table and column names
    You can turn off this feature to get a quicker startup with -A

    Database changed
    ```

## Implémenter la base de données

!!! info "Créer sa propre base de données"
    Pour interfacer notre base de données qui pour le moment est vide. Pour cela nous allons utiliser le fichier qui contient les commandes sql et ce directement depuis la ligne de commande, donc sans lancer mysql.

    Vous avez récupéré ce fichier lors du clonage de l'archive git, il se nomme `cooperative.sql`. 
    
    En se plaçant dans ce répertoire :

    ```sh
    mysql -u nom_utilisateur -p nom_base < cooperative.sql
    ```

    Dérouler le code ci-dessous pour examiner le code SQL contenu dans ce fichier :

    ??? note "Code SQL de cooperative.sql"
        ```sql
        CREATE TABLE `buveurs` (
        `email` varchar(50) PRIMARY KEY NOT NULL,
        `nom_buveur` varchar(50) NOT NULL,
        `prenom_buveur` varchar(50) NOT NULL,
        `mot_de_passe` int(11) NOT NULL,
        `adresse` varchar(30)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

        INSERT INTO `buveurs` (`email`, `nom_buveur`, `prenom_buveur`, `mot_de_passe`, `adresse`) VALUES
        ('thibault.macquart@egd.mg', 'Thibault', 'Macquart', 12345, ''),
        ('Ostivekevin@gmail.com', 'Randrianomenjanahary', 'Ostive', 1234, 'Madagascar');

        CREATE TABLE `viticulteurs` (
        `nvt` int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
        `nom_viticulteur` varchar(50) NOT NULL,
        `ville_viticulteur` varchar(50)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

        INSERT INTO `viticulteurs` (`nvt`, `nom_viticulteur`, `ville_viticulteur`) VALUES
        (1, 'Rothschild', 'Bordeaux'),
        (2, 'Dylan', 'Lyon');

        CREATE TABLE `vins` (
        `nv` int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
        `cru` varchar(50) NOT NULL,
        `annee` int(11) NOT NULL,
        `degre` float NOT NULL,
        `stock` int(11) NOT NULL,
        `nvt` int(11) NOT NULL,
        FOREIGN KEY (`nvt`) REFERENCES `viticulteurs`(`nvt`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

        INSERT INTO `vins` (`nv`, `cru`, `annee`, `degre`, `stock`, `nvt`) VALUES
        (1, 'Chateau_Latour', 2001, 11.8, 63, 1),
        (2, 'Moulin_à_vent', 2010, 12.2, 48, 2);

        CREATE TABLE `commande` (
        `nv` int(11) NOT NULL,
        `email` varchar(50) NOT NULL,
        `date_commande` date NOT NULL,
        `livraison_commande` tinyint(1) NOT NULL,
        PRIMARY KEY (`nv`,`email`),
        FOREIGN KEY (`email`) REFERENCES `buveurs`(`email`),
        FOREIGN KEY (`nv`) REFERENCES `vins`(`nv`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
        ```

    En étudiant le code SQL qui a permis de créer la base de la coopérative, essayez de répondre aux questions ci dessous :

    !!! question "Question 1"

        === "Question"
            Combien d'attributs posséde la table `buveurs` ? Quelle est la clef primaire de cette table ?

        === "Réponse"
            La table `buveurs` contient 5 attributs et l'attribut `email` sert de clef primaire.

    !!! question "Question 2"

        === "Question"
            Quel est le mot de passe du buveur `Macquart` ? 
        === "Réponse"
            Le mot de passe est `12345`

    !!! question "Question 3"

        === "Question"
            Comment peut-on savoir quel est le viticulteur qui a produit le vin nommé `Moulin_à_vent` ? Quelle requête SQL faudrait-il  formuler ?

        === "Réponse"
            Il y a une clef étrangére dans la table `vins` qui référence le viticulteur par son identifiant :
            ```sql
            SELECT nom_viticulteur FROM viticulteurs
            INNER JOIN vins ON viticulteurs.nvt = vins.nvt
            WHERE vins.cru = "Moulin_à_vent" ;
            ```

    !!! question "Question 4"

        === "Question"
            En regardant le code de la création de la table `commande`, est-il possible pour un même client de passer plusieurs commande chez d'un même vin ?

        === "Réponse"
            Non c'est impossible,en effet la table commande possède une clef primaire `(nv,email)` où `nv` est l'identifiant du vin  et `email`, l'identifiant du client.

            Cette clef doit être unique donc un client ne peux passer qu'une seule commande d'un même vin, ce qui peut  poser de gros problèmes si le client veut effectuer une nouvelle commande.







